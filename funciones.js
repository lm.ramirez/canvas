function inicializar (){

    let lienzo=document.getElementById("miLienzo");
    let ctx=lienzo.getContext('2d');
    lienzo.clientWidth=500;
    lienzo.height=500;

    ctx.fillStyle = 'green';
    ctx.fillRect(10, 10, 100, 100);

    ctx.beginPath();
    ctx.arc(75,75,50,0,Math.PI*2,true); // Círculo externo
    ctx.moveTo(110,75);
    ctx.stroke();
}